/*
 * Copyright 2018 Red Eléctrica de España, S.A.U.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Red Eléctrica de España, S.A.U. as the copyright owner of
 * the program.
 */

package es.ree.eemws.client.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.soap.SOAPFaultException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import ch.iec.tc57._2011.schema.message.HeaderType;
import ch.iec.tc57._2011.schema.message.PayloadType;
import ch.iec.tc57._2011.schema.message.RequestMessage;
import ch.iec.tc57._2011.schema.message.ResponseMessage;
import es.ree.eemws.core.utils.error.EnumErrorCatalog;
import es.ree.eemws.core.utils.error.ErrorMessages;
import es.ree.eemws.core.utils.iec61968100.EnumMessageStatus;
import es.ree.eemws.core.utils.iec61968100.EnumNoun;
import es.ree.eemws.core.utils.iec61968100.EnumVerb;
import es.ree.eemws.core.utils.iec61968100.MessageMetaData;
import es.ree.eemws.core.utils.operations.HandlerException;
import es.ree.eemws.core.utils.security.SignatureManager;
import es.ree.eemws.core.utils.security.SignatureManagerException;
import es.ree.eemws.core.utils.security.SignatureSyntaxException;
import es.ree.eemws.core.utils.security.SignatureVerificationException;
import es.ree.eemws.core.utils.xml.XMLElementUtil;
import es.ree.eemws.core.utils.xml.XMLUtil;

/**
 * Parent class for all client's operations.
 *
 * @author Red Eléctrica de España S.A.U.
 * @version 2.0 01/01/2018
 */
public abstract class ParentClient {
     
    /** Web service's URL endpoint . */
    private URL endPoint = null;

    /** Flag to sign the request. */
    private boolean signRequest = true;

    /** Flag to verify response's signature. */
    private boolean verifyResponse = true;

    /** Certificate to sign request. */
    private X509Certificate certificate = null;

    /** Private key of the certificate. */
    private PrivateKey privateKey = null;
 
    /** Message metadata. */
    private MessageMetaData messageMetaData = new MessageMetaData();
    
    /**
     * Sets the URL of the end point of the web service.
     * @param url URL of the end point of the web service.
     * @throws MalformedURLException if the given address is not valid.
     */
    public final void setEndPoint(final String url) throws MalformedURLException {

        endPoint = new URL(url);
    }

    /**
     * Sets the URL of the end point of the web service.
     * @param url URL of the end point of the web service.
     */
    public final void setEndPoint(final URL url) {

        endPoint = url;
    }

    /**
     * Sets whether the request has to be signed.
     * @param flag <code>true</code> if request has to be signed.
     */
    public final void setSignRequest(final boolean flag) {

        signRequest = flag;
    }

    /**
     * Sets whether the response's signature has to be validated.
     * @param flag <code>true</code> if the response's signature has to be validated.
     */
    public final void setVerifyResponse(final boolean flag) {

        verifyResponse = flag;
    }

    /**
     * Sets the certificate to sign the request.
     * @param inCertificate Certificate to sign request.
     */
    public final void setCertificate(final X509Certificate inCertificate) {

        certificate = inCertificate;
    }

    /**
     * Sets the private key to sign the request.
     * @param inPrivateKey Private key to sign the request.
     */
    public final void setPrivateKey(final PrivateKey inPrivateKey) {

        privateKey = inPrivateKey;
    }

    /**
     * Gets message's metadata. Metadata holds information that is not
     * present neither in the request nor in the response such as certificate information or status.
     * @return Message's metadata.
     */
    public final MessageMetaData getMessageMetaData() {
        return messageMetaData;
    }
    
    /**
     * Sends the given request message to the configured URL.
     * Note: This is not the most clever implementations, but it works. Please, refer to versions &lt; 1.2.1 to
     * get a better (by far) implementation. This implements a workarround for a Java 1.8.144 bug that it is
     * supposed to be fixed in version 10. We have developed this workarround using the very interfaces that
     * the previous version had. Note that there is no handlers anymore, but we still throws "HanderException".
     * @param message Message to send.
     * @return Response to the message.
     * @throws HandlerException If it is not possible to send the message or if the received response has errors.
     */
    protected final ResponseMessage sendMessage(final RequestMessage message) throws HandlerException {
  
        ResponseMessage response = null;
        messageMetaData = new MessageMetaData();
        
        try {
  
            Document requestMessBody = XMLUtil.string2Document(XMLElementUtil.object2StringBuilder(message));

            if (signRequest) {
                if (certificate != null && privateKey != null) {
                    SignatureManager.signDocument(requestMessBody, privateKey, certificate);
                } else {
                    SignatureManager.signDocument(requestMessBody);
                }
            }

            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = soapConnectionFactory.createConnection();
            
            MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
            
            SOAPMessage soapMessage = factory.createMessage();
            soapMessage.getSOAPBody().removeContents();
            soapMessage.getSOAPBody().addDocument(requestMessBody);
                        
            SOAPMessage soapRespon = connection.call(soapMessage, endPoint);
            connection.close();
            
            /* From now on, we do not need this (possibly huge) document. */
            requestMessBody = null;
            
            String body = XMLUtil.getNodeValue("Body", new StringBuilder(XMLUtil.soapMessage2String(soapRespon))); //$NON-NLS-1$

            if (soapRespon.getSOAPPart().getEnvelope().getBody().hasFault()) {
                SOAPFault fault = soapRespon.getSOAPPart().getEnvelope().getBody().getFault();
                
                messageMetaData.setStatus(EnumMessageStatus.FAILED);
                messageMetaData.setServerTimestamp(Calendar.getInstance());
                messageMetaData.setRejectText(fault.getFaultString());
                
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_010, new SOAPFaultException(fault), fault.getFaultString());
               
            }
            
            response = (ResponseMessage) XMLElementUtil.element2Obj(XMLElementUtil.string2Element(body), ResponseMessage.class);

            messageMetaData.setStatus(EnumMessageStatus.fromString(response.getReply().getResult()));
            messageMetaData.setServerTimestamp(Calendar.getInstance());
            
            if (verifyResponse) { 
                X509Certificate x509Certificate = SignatureManager.verifyString(new StringBuilder(body));
                messageMetaData.setSignatureCertificate(x509Certificate);
            } 
            
        } catch (SignatureVerificationException e) {
            
            HandlerException he = new HandlerException(EnumErrorCatalog.ERR_HAND_007, e);
            messageMetaData.setSignatureCertificate(e.getDetails().getSignatureCertificate());
            messageMetaData.setException(he);
            throw he;

        } catch (SignatureSyntaxException e) {

            HandlerException he = new HandlerException(EnumErrorCatalog.ERR_HAND_008, e);
            messageMetaData.setException(he);
            throw he;

        } catch (SignatureManagerException e) {
            
            HandlerException he = new HandlerException(EnumErrorCatalog.ERR_HAND_009, e);
            messageMetaData.setException(he);
            throw he;

        } catch (ParserConfigurationException | SAXException e) {
            
            HandlerException he = new HandlerException(EnumErrorCatalog.ERR_HAND_004, e);
            messageMetaData.setException(he);
            throw he;
            
        } catch (IOException | SOAPException | RuntimeException e) {
            StringBuffer errStr = new StringBuffer();
            
            Throwable ex = e;
            while (ex != null) {
                errStr.append(ex.getMessage());
                ex = ex.getCause();
            }
            
            if (errStr.indexOf("trustAnchors") != -1 || errStr.indexOf("PKIX path building failed") != -1) { //$NON-NLS-1$ //$NON-NLS-2$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_013);
            } else if (errStr.indexOf("No subject alternative") != -1) {  //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_014);
            } else if (errStr.indexOf("UnknownHostException") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_015);
            } else if (errStr.indexOf("404") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_016);
            } else if (errStr.indexOf("403") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_017);
            } else if (errStr.indexOf("401") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_018);
            } else if (errStr.indexOf("200") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_019);
            } else if (errStr.indexOf("Connection refused") != -1 || errStr.indexOf("Connection timed out") != -1) { //$NON-NLS-1$ //$NON-NLS-2$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_020);
            } else if (errStr.indexOf("The server sent HTTP status code 400:") != -1) { //$NON-NLS-1$ 
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_021);
            } else if (errStr.indexOf("unrecognized_name") != -1) { //$NON-NLS-1$
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_023);
            } else {
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_022, e);
            }
        
        
        } catch ( JAXBException  e) {
        
            throw new HandlerException(EnumErrorCatalog.ERR_HAND_004, e);
       
        } 
        
        return response;
    }

  
    /**
     * Validates the received response.
     * @param responseMessage Received response.
     * @param expectedNoun Expected noun, according to the operation.
     * @throws HandlerException If the response is not valid.
     */
    protected void validateResponse(final ResponseMessage responseMessage, final String expectedNoun) throws HandlerException {
        validateResponse(responseMessage, expectedNoun, false);
    }

    /**
     * Validates the received response.
     * @param responseMessage Received response.
     * @param canBeEmpty <code>true</code> if the response can be empty.
     * @throws HandlerException If the response is not valid.
     */
    protected void validateResponse(final ResponseMessage responseMessage, final boolean canBeEmpty) throws HandlerException {
        validateResponse(responseMessage, null, canBeEmpty);
    }

    /**
     * Validates the received response.
     * <li>Must have a header.
     * <li>Must have a payload
     * <li>The verb must be "reply"
     * <li>Noun must match with the given one and if no noun is provided, must match with the root tag.
     * @param responseMessage Received response.
     * @param expectedNoun Expected noun, according to the operation.
     * @param canBeEmpty <code>true</code> if the server can return an empty response (only for "put" operations)
     * @throws HandlerException If the response is not valid (has no header, noun + verb missmatch, etc.)
     */
    private void validateResponse(final ResponseMessage responseMessage, final String expectedNoun, final boolean canBeEmpty) throws HandlerException {

        HeaderType header = responseMessage.getHeader();

        if (header == null) {
            throw new HandlerException(EnumErrorCatalog.ERR_HAND_002, ErrorMessages.NO_HEADER);
        }

        String verb = header.getVerb();
        String noun = header.getNoun();

        if (expectedNoun != null && !expectedNoun.equals(noun)) {
            throw new HandlerException(EnumErrorCatalog.ERR_HAND_012, verb, noun, EnumVerb.REPLY.toString(), expectedNoun);
        }

        PayloadType payload = responseMessage.getPayload();

        if (payload == null) {
            throw new HandlerException(EnumErrorCatalog.ERR_HAND_011);
        }

        List<Element> anies = payload.getAnies();
        boolean compressed = payload.getCompressed() != null;
        boolean empty = anies.isEmpty() && !compressed;

        if (empty) {
            if (canBeEmpty) {
                if (!EnumVerb.REPLY.toString().equals(verb)) {
                    throw new HandlerException(EnumErrorCatalog.ERR_HAND_012, verb, ErrorMessages.NO_NOUN_EXPECTED, EnumVerb.REPLY.toString(), ErrorMessages.NO_NOUN_EXPECTED);
                }
            } else {
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_011);
            }
        } else {

            String rootTag;
            if (compressed) {
                rootTag = EnumNoun.COMPRESSED.toString();
            } else {

                Element message = anies.get(0);
                if (message == null) {
                    throw new HandlerException(EnumErrorCatalog.ERR_HAND_011);
                }
                rootTag = message.getLocalName();
            }

            if (!EnumVerb.REPLY.toString().equals(verb) || !rootTag.equals(noun)) {
                throw new HandlerException(EnumErrorCatalog.ERR_HAND_012, verb, noun, EnumVerb.REPLY.toString(), rootTag);
            }
        }
    }

}
